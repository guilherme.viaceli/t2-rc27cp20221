# Trabalho 2: Integração de habilidades - 2022/1
Disciplina: Redes de Computadores
Curso: Engenharia de Computação / Elétrica
Nome: Guilherme Viaceli                                         RA: 1500228


## Tarefa 1:  Sub-Redes
| Sub- Rede |             IPv6 - Sub-Rede            |  IPv4 - Sub-Rede  |  IPv4 - Máscara   | IPv4 - Broadcast  |    
|:---------:|:--------------------------------------:|:-----------------:|:-----------------:|:-----------------:|
| Matriz    | 2001:DB8:ACAD:1C00::/64 | 200.200.28.0   | 255.255.255.192 | 200.200.28.63  |
| Filial 1  | 2001:DB8:ACAD:1C01::/64 | 200.200.28.64  | 255.255.255.224 | 200.200.28.95  |
| Filial 2  | 2001:DB8:ACAD:1C02::/64 | 200.200.28.96  | 255.255.255.224 | 200.200.28.127 |
| Filial 3  | 2001:DB8:ACAD:1C03::/64 | 200.200.28.128 | 255.255.255.224 | 200.200.28.159 |
| Filial 4  | 2001:DB8:ACAD:1C04::/64 | 200.200.28.160 | 255.255.255.224 | 200.200.28.192 |
| Filial 5  | 2001:DB8:ACAD:1C05::/64 | 200.200.28.192 | 255.255.255.224 | 200.200.28.223 |
| pb-vit    | 2001:DB8:ACAD:1CFF::0:0/112 | 200.200.28.224 | 255.255.255.252 | 200.200.28.227 |
| vit-fb    | 2001:DB8:ACAD:1CFF::1:0/112 | 200.200.28.228 | 255.255.255.252 | 200.200.28.231 |
| fb-ita    | 2001:DB8:ACAD:1CFF::2:0/112 | 200.200.28.232 | 255.255.255.252 | 200.200.28.235 |
| ita-pb    | 2001:DB8:ACAD:1CFF::3:0/112 | 200.200.28.236 | 255.255.255.252 | 200.200.28.239 |
| cv-ita    | 2001:DB8:ACAD:1CFF::4:0/112  | 200.200.28.240 | 255.255.255.252 | 200.200.28.243 |


## Tarefa 2: Endereçamento de Dispositivos
| Dispositivo |    Interface   |  IPv4  |  IPv4 - Máscara | IPv4 - Gateway   | IPv6 / Prefixo (GUA) |  IPv6 - Gateway |IPv6 (LLA) |
|:-----------:|:--------------:|:-----------------:|:-----------------:|:--------------------:|:--------------------:|:--------------------:|:--------------------:|
| PC1    | NIC | 200.200.28.3   | 255.255.255.192  | 200.200.28.1 | 2001:DB8:ACAD:1C00::3/64 |FE80::1|EUI-64|
| PC2    | NIC | 200.200.28.4   | 255.255.255.192 | 200.200.28.1 | 2001:DB8:ACAD:1C00::4/64 |FE80::1|EUI-64|
| PC3    | NIC | 200.200.28.67   | 255.255.255.224  | 200.200.28.65 | 2001:DB8:ACAD:1C01::3/64 |FE80::1|EUI-64|
| PC4    | NIC | 200.200.28.68   |  255.255.255.224 | 200.200.28.65  | 2001:DB8:ACAD:1C01::4/64 |FE80::1|EUI-64|
| PC5    | NIC | 200.200.28.99   | 255.255.255.224  | 200.200.28.97 | 2001:DB8:ACAD:1C02::3/64 |FE80::1|EUI-64|
| PC6    | NIC | 200.200.28.100   | 255.255.255.224 | 200.200.28.97 | 2001:DB8:ACAD:1C02::4/64 |FE80::1|EUI-64|
| Switch-Matriz  | SVI | 200.200.28.2  | 255.255.255.192 | 200.200.28.1 | ||
| Switch-Filial1 | SVI | 200.200.28.66  | 255.255.255.224 | 200.200.28.65 | ||
| Switch-Filial2 | SVI | 200.200.28.98  | 255.255.255.224 | 200.200.28.97 |  ||
| Roteador Pato Branco    | Fa0/0 |  200.200.28.1 | 255.255.255.192 |  | 2001:DB8:ACAD:1C00::1/64 ||FE80::1|
| Roteador Pato Branco    | Se0/0/0 |  200.200.28.225 | 255.255.255.252   |  |2001:DB8:ACAD:1CFF::0:1/112 ||EUI-64|
| Roteador Pato Branco    | Se0/0/1 | 200.200.28.238 | 255.255.255.252   |  |2001:DB8:ACAD:1CFF::3:2/112 ||EUI-64|
| Roteador Francisco Beltrão   | Fa0/0 | 200.200.28.65    | 255.255.255.224  |  | 2001:DB8:ACAD:1C01::1/64 ||FE80::1|
| Roteador Francisco Beltrão    | Se0/0/0 | 200.200.28.233   | 255.255.255.252  | |2001:DB8:ACAD:1CFF::2:1/112 | |EUI-64|
| Roteador Francisco Beltrão    | Se0/0/1 |  200.200.28.230  | 255.255.255.252 |  | 2001:DB8:ACAD:1CFF::1:2/112 ||EUI-64|
| Roteador Vitorino    | Se0/0/0 | 200.200.28.229   | 255.255.255.252 | |2001:DB8:ACAD:1CFF::1:1/112| |EUI-64|
| Roteador Vitorino    | Se0/0/1 | 200.200.28.226   | 255.255.255.252 |  |2001:DB8:ACAD:1CFF::0:2/112||EUI-64|
| Roteador Itapejara d'Oeste    | Se0/0/0 | 200.200.28.237   | 255.255.255.252 | |2001:DB8:ACAD:1CFF::3:1/112 ||EUI-64|
| Roteador Itapejara d'Oeste    | Se0/0/1 |  200.200.28.234  | 255.255.255.252  |  |2001:DB8:ACAD:1CFF::2:2/112 ||EUI-64|
| Roteador Itapejara d'Oeste    | Fa0/1 |  200.200.28.241  | 255.255.255.252 |  |2001:DB8:ACAD:1CFF::4:1/112 ||EUI-64|
| Roteador Coronel Vivida    | Fa0/0 |  200.200.28.97  | 255.255.255.224  |  | 2001:DB8:ACAD:1C02::1/64 ||FE80::1|
| Roteador Coronel Vivida    | Fa0/1 | 200.200.28.242	   | 255.255.255.252 |  | 2001:DB8:ACAD:1CFF::4:2/112 ||EUI-64|
## Tarefa 3: Tabela de Roteamento

## Roteador Pato Branco     
## IPv4    
| Rede de Destino  |  IPv4 - Máscara   | Next Hop| Interface de Saída |
|:-----------:|:--------------:|:-----------------:|:-----------------:|
| 200.200.28.64     | 255.255.255.224    |  200.200.28.226  | Se0/0/0 |
| 200.200.28.228    | 255.255.255.252 	 |  200.200.28.226  | Se0/0/0 |
| 200.200.28.232    | 255.255.255.252 	 |  200.200.28.226  | Se0/0/0 |
| 200.200.28.240    | 255.255.255.252 	 |  200.200.28.226  | Se0/0/0 |
| 200.200.28.96	    | 255.255.255.224	 |  200.200.28.226  | Se0/0/0 |
## IPv6    
| Rede de Destino / Prefixo | Next Hop| Interface de Saída |
|:-----------:|:-----------------:|:-----------------:|
| 2001:DB8:ACAD:1CFF::2:0/112	 | 2001:DB8:ACAD:1CFF::0:2 |Se0/0/0    |  
| 2001:DB8:ACAD:1C01::/64        | 2001:DB8:ACAD:1CFF::0:2 |Se0/0/0    | 
| 2001:DB8:ACAD:1CFF::1:0/112    | 2001:DB8:ACAD:1CFF::0:2 |Se0/0/0    |  
| 2001:DB8:ACAD:1CFF::4:0/112    | 2001:DB8:ACAD:1CFF::0:2 |Se0/0/0    |  
| 2001:DB8:ACAD:1C02::/64        | 2001:DB8:ACAD:1CFF::0:2  |Se0/0/0    |  
## Roteador Francisco Beltrão     
## IPv4    
| Rede de Destino  |  IPv4 - Máscara   | Next Hop| Interface de Saída |
|:-----------:|:--------------:|:-----------------:|:-----------------:|
| 200.200.28.240  | 255.255.255.252  | 200.200.28.234   | Se0/0/0 |
| 200.200.28.96   | 255.255.255.224  | 200.200.28.234   | Se0/0/0 |
| 200.200.28.236  | 255.255.255.252  | 200.200.28.234   | Se0/0/0 |
| 200.200.28.0    | 255.255.255.192  | 200.200.28.234   | Se0/0/0 |
| 200.200.28.224  | 255.255.255.252  | 200.200.28.234   | Se0/0/0 |
## IPv6    
| Rede de Destino / Prefixo | Next Hop| Interface de Saída |
|:-----------:|:-----------------:|:-----------------:|
| 2001:DB8:ACAD:1CFF::0:0/112    | 2001:DB8:ACAD:1CFF::2:2  | Se0/0/0   |  
| 2001:DB8:ACAD:1C02::/64       | 2001:DB8:ACAD:1CFF::2:2  | Se0/0/0   | 
| 2001:DB8:ACAD:1CFF::4:0/112  | 2001:DB8:ACAD:1CFF::2:2  | Se0/0/0   |  
| 2001:DB8:ACAD:1C00::/64      | 2001:DB8:ACAD:1CFF::2:2  | Se0/0/0   |  
| 2001:DB8:ACAD:1CFF::3:0/112     | 2001:DB8:ACAD:1CFF::2:2  | Se0/0/0   |  
## Roteador Vitorino     
## IPv4    
| Rede de Destino  |  IPv4 - Máscara   | Next Hop| Interface de Saída |
|:-----------:|:--------------:|:-----------------:|:-----------------:|
| 200.200.28.0    | 255.255.255.192  | 200.200.28.230   | Se0/0/0 |
| 200.200.28.64   | 255.255.255.224  | 200.200.28.230   | Se0/0/0 |
| 200.200.28.96   | 255.255.255.224  | 200.200.28.230   | Se0/0/0 |
| 200.200.28.232  | 255.255.255.252  | 200.200.28.230   | Se0/0/0 |
| 200.200.28.236  | 255.255.255.252  | 200.200.28.230   | Se0/0/0 |
| 200.200.28.240  | 255.255.255.252  | 200.200.28.230   | Se0/0/0 |
## IPv6    
| Rede de Destino / Prefixo | Next Hop| Interface de Saída |
|:-----------:|:-----------------:|:-----------------:|
| 2001:DB8:ACAD:1C00::/64 	  | 2001:DB8:ACAD:1CFF::1:2 |  Se0/0/0  |  
| 2001:DB8:ACAD:1C01::/64    | 2001:DB8:ACAD:1CFF::1:2 |  Se0/0/0  | 
| 2001:DB8:ACAD:1C02::/64     | 2001:DB8:ACAD:1CFF::1:2 |  Se0/0/0  |  
| 2001:DB8:ACAD:1CFF::3:0/112 | 2001:DB8:ACAD:1CFF::1:2 |  Se0/0/0  |  
| 2001:DB8:ACAD:1CFF::4:0/112 | 2001:DB8:ACAD:1CFF::1:2 |  Se0/0/0  | 
| 2001:DB8:ACAD:1CFF::2:0/112 | 2001:DB8:ACAD:1CFF::1:2 |  Se0/0/0  | 
## Roteador Itapejara d'Oeste     
## IPv4    
| Rede de Destino  |  IPv4 - Máscara   | Next Hop| Interface de Saída |
|:-----------:|:--------------:|:-----------------:|:-----------------:|
| 200.200.28.0    | 255.255.255.192 | 200.200.28.238   | Se0/0/0 |
| 200.200.28.64   | 255.255.255.224 | 200.200.28.238   | Se0/0/0 |
| 200.200.28.96   | 255.255.255.224 | 200.200.28.242   | Fa0/1   |
| 200.200.28.224  | 255.255.255.252	| 200.200.28.238   | Se0/0/0 |
| 200.200.28.228  | 255.255.255.252	| 200.200.28.238   | Se0/0/0 |
## IPv6    
| Rede de Destino / Prefixo | Next Hop| Interface de Saída |
|:-----------:|:-----------------:|:-----------------:|
| 2001:DB8:ACAD:1C00::/64	  | 2001:DB8:ACAD:1CFF::3:2  | Se0/0/0   |  
| 2001:DB8:ACAD:1C01::/64    | 2001:DB8:ACAD:1CFF::3:2	 | Se0/0/0   | 
| 2001:DB8:ACAD:1C02::/64    | 2001:DB8:ACAD:1CFF::4:2	 | Fa0/1     |  
| 2001:DB8:ACAD:1CFF::0:0/112   | 2001:DB8:ACAD:1CFF::3:2	 | Se0/0/0   |  
| 2001:DB8:ACAD:1CFF::1:0/112  | 2001:DB8:ACAD:1CFF::3:2	 | Se0/0/0   |  
## Roteador Coronel Vivida     
## IPv4    
| Rede de Destino  |  IPv4 - Máscara   | Next Hop| Interface de Saída |
|:-----------:|:--------------:|:-----------------:|:-----------------:|
| 200.200.28.0	   | 255.255.255.192 | 200.200.28.241   | Fa0/1 |
| 200.200.28.64    | 255.255.255.224 | 200.200.28.241   | Fa0/1 |
| 200.200.28.224   | 255.255.255.252 | 200.200.28.241   | Fa0/1 |
| 200.200.28.228   | 255.255.255.252 | 200.200.28.241   | Fa0/1 |
| 200.200.28.232   | 255.255.255.252 | 200.200.28.241   | Fa0/1 |
| 200.200.28.236   | 255.255.255.252 | 200.200.28.241   | Fa0/1 |
## IPv6    
| Rede de Destino / Prefixo | Next Hop| Interface de Saída |
|:-----------:|:-----------------:|:-----------------:|
| 2001:DB8:ACAD:1C00::/64    | 2001:DB8:ACAD:1CFF::4:1	| Fa0/1   |  
| 2001:DB8:ACAD:1C01::/64    | 2001:DB8:ACAD:1CFF::4:1 | Fa0/1   | 
| 2001:DB8:ACAD:1CFF::0:0/112   | 2001:DB8:ACAD:1CFF::4:1 | Fa0/1   |  
| 2001:DB8:ACAD:1CFF::1:0/112 | 2001:DB8:ACAD:1CFF::4:1 | Fa0/1   |  
| 2001:DB8:ACAD:1CFF::2:0/112 | 2001:DB8:ACAD:1CFF::4:1 | Fa0/1   | 
| 2001:DB8:ACAD:1CFF::3:0/112 | 2001:DB8:ACAD:1CFF::4:1 | Fa0/1   | 

#### Topologia - Packet Tracer
[Topologia - Packet Tracer](https://gitlab.com/guilherme.viaceli/t2-rc27cp20221/-/blob/ce19e845d9f1b2d8e8b6787a46f9c037bcf16cf8/T2-GuilhermeViaceli.pkt)

#### Arquivos de Configuração dos Dispositivos Intermediários (roteadores e switches)

- [ ] [Roteador Coronel Vivida](https://gitlab.com/guilherme.viaceli/t2-rc27cp20221/-/blob/main/r-cv-gv.txt)
- [ ] [Roteador Francisco Beltrão](https://gitlab.com/guilherme.viaceli/t2-rc27cp20221/-/blob/main/r-fb-gv.txt)
- [ ] [Roteador Itapejara doeste](https://gitlab.com/guilherme.viaceli/t2-rc27cp20221/-/blob/main/r-ita-gv.txt)
- [ ] [Roteador Pato Branco](https://gitlab.com/guilherme.viaceli/t2-rc27cp20221/-/blob/main/r-pb-gv.txt)
- [ ] [Roteador Vitorino](https://gitlab.com/guilherme.viaceli/t2-rc27cp20221/-/blob/main/r-vit-gv.txt)
- [ ] [Switch Matriz](https://gitlab.com/guilherme.viaceli/t2-rc27cp20221/-/blob/main/sw-matriz-gv.txt)
- [ ] [Switch Filial 1](https://gitlab.com/guilherme.viaceli/t2-rc27cp20221/-/blob/main/sw-filial1-gv.txt)
- [ ] [Switch Filial 2](https://gitlab.com/guilherme.viaceli/t2-rc27cp20221/-/blob/main/sw-filial2-gv.txt)
