!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
security passwords min-length 10
!
hostname r-pb-gv
!
login block-for 120 attempts 3 within 60
!
!
enable secret 5 $1$mERr$4bSo.y6c8x.2QjVpmNjuQ0
!
!
!
!
!
!
no ip cef
ipv6 unicast-routing
!
no ipv6 cef
!
!
!
username guilherme secret 5 $1$mERr$F5V/u2120qbEqnDJcxLe00
!
!
license udi pid CISCO2811/K9 sn FTX101754WN-
!
!
!
!
!
!
!
!
!
ip domain-name guilherme.viaceli.com.br
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 description pb-matriz
 ip address 200.200.28.1 255.255.255.192
 duplex auto
 speed auto
 ipv6 address FE80::1 link-local
 ipv6 address 2001:DB8:ACAD:1C00::1/64
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface Serial0/0/0
 description pb-vit
 ip address 200.200.28.225 255.255.255.252
 ipv6 address 2001:DB8:ACAD:1CFF::1/112
 clock rate 56000
!
interface Serial0/0/1
 description pb-ita
 ip address 200.200.28.238 255.255.255.252
 ipv6 address 2001:DB8:ACAD:1CFF::3:2/112
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
ip route 200.200.28.64 255.255.255.224 200.200.28.226 
ip route 200.200.28.228 255.255.255.252 200.200.28.226 
ip route 200.200.28.232 255.255.255.252 200.200.28.226 
ip route 200.200.28.240 255.255.255.252 200.200.28.226 
ip route 200.200.28.96 255.255.255.224 200.200.28.226 
!
ip flow-export version 9
!
ipv6 route 2001:DB8:ACAD:1CFF::2:0/112 2001:DB8:ACAD:1CFF::2
ipv6 route 2001:DB8:ACAD:1C01::/64 2001:DB8:ACAD:1CFF::2
ipv6 route 2001:DB8:ACAD:1CFF::1:0/112 2001:DB8:ACAD:1CFF::2
ipv6 route 2001:DB8:ACAD:1CFF::4:0/112 2001:DB8:ACAD:1CFF::2
ipv6 route 2001:DB8:ACAD:1C02::/64 2001:DB8:ACAD:1CFF::2
!
ip access-list extended sl_def_acl
 deny tcp any any eq telnet
 deny tcp any any eq www
 deny tcp any any eq 22
 permit tcp any any eq 22
!
banner motd % Acesso restrito aos alunos da Disciplina Redes de Computadores - 2022/1. Administrador: Guilherme Viaceli. %
!
!
!
!
!
line con 0
 exec-timeout 5 0
 password @Cons-guilherme
 login
!
line aux 0
!
line vty 0 4
 exec-timeout 5 0
 password redes1500228
 login
 transport input ssh
line vty 5 15
 exec-timeout 5 0
 login
 transport input ssh
!
!
!
end

